Prerequisites
This section will give you some information about what tools you will need. You can skip to the Installation section to start installing the template. We already mentioned all the prerequisites and how to install them in the Installation section.

1.Yeoman generator for AngularJS with GulpJS
The project uses Yeoman generator for AngularJS with GulpJS for quickly bootstrapping the application. You can check out the link for more detailed information.

Simply, Yeoman generator for AngularJS with GulpJS will automatically inject any templates, controllers, modules, directives and other application related files into the index.html file for you. It will also automatically find and compile scss files into one css file and inject that one as well.

It's really easy to develop your Angular applications with the generator. We are highly recommending you to check out the above link and learn more about it before getting started.

2.Node.js
To install and use the project, you will need Node.js installed to your computer. We won't get into too much detail about Node.js as it's out of the scope of this documentation. Also you won't need to actually use Node.js, it's only required for the development process.

3.Git
To be able to install and use the project, you will also need Git installed to your computer. Git is required for bower to work correctly.

4.Bower
The project uses Bower package manager to install and manage 3rd party components and libraries.

5.Gulp
Gulp is required for the project to work properly. Although there are ways to develop the project without using Gulp or Node.js, we strongly recommend you to use them in order to take advantage of the true power.

Installation
A. Installing Prerequisites
  Download and install the latest Node.js from its web site.
  Download and install the latest Git from its web site.
  Open your favorite console application (Terminal, Command Prompt etc.), run the following command and wait for it to finish:
    npm install -g bower
  Run the following command and wait for it to finish:
    npm install -g gulp
  Now you are ready to install.
  
B. Installing
  Unzip the zip folder that you have downloaded.
  Extract the contents of the zip file (west-pokot-project.zip) into a folder that you will work within.
  Open your favorite console application (Terminal, Command Prompt etc.), navigate into your work folder, run the following command and wait for it to finish:
    npm install
  This command will install all the required Node.js modules into the node_modules directory inside your work folder.

  Note: Installing Node.js packages may throw a lot of warnings and errors along the way. As long as the process finishes without any error notes such as "Killed", you will be fine.
  
  Run the following command and wait it for to finish:
    bower install
  This command will install all the required bower packages into the bower_components directory inside your work folder.
  
  And now, you are ready to run the west-pokot-project for the first time.
  
  C. Running
  While still in your work folder, run the following command in the console application:
    gulp serve
  And that's it. Gulp will take care everything and start the west-pokot-project. Your default browser will be opened automatically, and you will be able to navigate through the template.  


