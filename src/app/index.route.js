(function ()
{
    'use strict';

    angular
        .module('west-pokot')
        // .config(routeConfig)

        .config(function ($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {

            $locationProvider.html5Mode(true);

	        // for now use this but we can also test with /login route
	        // the token is checked and if doesnt exist it goes back to login
            $urlRouterProvider.otherwise('/dashboard');

            /*
             *Used in angular when dealing with CORS.
             *Set up cross domain requests with the $http service.
             * You don't need to do this, because setting useXDomain to true has no effect
             */

            $httpProvider.defaults.useXDomain = true;
            delete $httpProvider.defaults.headers.common['X-Requested-With'];
            // $httpProvider.defaults.withCredentials = true;

            // Inject $cookies
            var $cookies;

            angular.injector(['ngCookies']).invoke([
                '$cookies', function (_$cookies)
                {
                    $cookies = _$cookies;
                }
            ]);

            // Get active layout
            var layoutStyle = $cookies.get('layoutStyle') || 'verticalNavigation';

            var layouts = {
                verticalNavigation  : {
                    main      : 'app/core/layouts/vertical-navigation.html',
                    toolbar   : 'app/toolbar/layouts/vertical-navigation/toolbar.html',
                    navigation: 'app/navigation/layouts/vertical-navigation/navigation.html'
                }
            };
            // END - Layout Style Switcher

            // State definitions
            $stateProvider
                .state('app', {
                    abstract: true,
                    views   : {
                        'main@'         : {
                            templateUrl: layouts[layoutStyle].main,
                            controller : 'MainController as vm'
                        },
                        'toolbar@app'   : {
                            templateUrl: layouts[layoutStyle].toolbar,
                            controller : 'ToolbarController as vm'
                        },
                        'navigation@app': {
                            templateUrl: layouts[layoutStyle].navigation,
                            controller : 'NavigationController as vm'
                        }
                    }
                });
        })
    
        .config(function($resourceProvider) {
            $resourceProvider.defaults.stripTrailingSlashes = false;
        })
	    .factory('authInterceptor', function ($rootScope, $q, $cookieStore, $location,$window,$localStorage) {

		    var authToken = $localStorage.token;

		    return {
			    request: function (config) {
				    config.headers = config.headers || {};

				    if (authToken) {

					    var today = new Date();
					    var payload = JSON.parse($window.atob(authToken.split('.')[1]));
					    var expDate = payload.exp;
						// the token has expired or not
					    if(expDate > today){

						    config.headers.Authorization = 'Bearer ' + authToken;

						    $rootScope.currentUser = angular.fromJson(payload.user);
					    }else{
						    $location.path('/login');
					    }

				    }else if ($location.path != '/login' && $location.path != '/') {
					    // delete $localStorage.token;
					    $location.path('/login');
				    }else{
					    delete $localStorage.token;
					    $location.path('/login');
				    }
				    return config;
			    },

			    // Intercept 401s and redirect you to login
			    responseError: function(response) {
				    if (response.status === 401 || response.status === 403) {
					    $location.path('/login');
					    delete $localStorage.token;
					    return $q.reject(response);
				    }else{
					    return $q.reject(response);
				    }

			    }
		    };

	    })
	    .config(function ($httpProvider) {
		    $httpProvider.interceptors.push('authInterceptor');
	    });
    
    /*to be solved later but use above*/
    /** @ngInject */
    function routeConfig($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider)
    {
    
    }

})();
