(function ()
{
    'use strict';

    /**
     * Main module of the Fuse
     */
    angular
        .module('west-pokot', [

            // Core
            'app.core',
            // Navigation
            'app.navigation',
            // Toolbar
            'app.toolbar',
            // Apps
            'app.westpokot',
            'ngStorage'
        ]);
})();
