(function ()
{
    'use strict';

    angular
        .module('app.code-settings.revenue-code')
        .controller('RevenueCodeController', RevenueCodeController);

    /** @ngInject */
    function RevenueCodeController(SampleData,$mdSidenav)
    {
        var vm = this;

        // Data
        vm.helloText = SampleData.data.helloText;
        vm.isShowStream = true;

        // Methods
        vm.showStreamFunc = showStreamFunc;
        vm.toggleSidenav = toggleSidenav;
        
        //////////
  
        /**
         * Toggles filter with true or empty string
         * @param filter
         */
        function showStreamFunc(showStream)
        {
            vm.isShowStream = showStream;
        }
    
        /**
         * Toggle sidenav
         *
         * @param sidenavId
         */
        function toggleSidenav(sidenavId)
        {
            $mdSidenav(sidenavId).toggle();
        }
    }
})();
