(function ()
{
    'use strict';

    angular
        .module('app.code-settings.revenue-code', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider
            .state('app.code-settings_revenue-code', {
                url    : '/revenue-code',
                views  : {
                    'content@app': {
                        templateUrl: 'app/main/verve/code-setting/revenue-code/revenue-code.html',
                        controller : 'RevenueCodeController as vm'
                    }
                },
                resolve: {
                    SampleData: function (apiResolver)
                    {
                        return apiResolver.resolve('sample@get');
                    }
                }
            });
    }
})();
