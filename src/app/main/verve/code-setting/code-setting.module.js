(function ()
{
    'use strict';

    angular
        .module('app.westpokot-code-settings', [
            'app.code-settings.revenue-code'
        ])
        .config(config);


    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msNavigationServiceProvider)
    {
  
        // Translation
        $translatePartialLoaderProvider.addPart('app/main/verve/code-setting');
        
        // Navigation
        msNavigationServiceProvider.saveItem('code-settings', {
            title : 'CODE SETTING',
            group : true,
            weight: 7
        });


        msNavigationServiceProvider.saveItem('code-settings.elements', {
            title : 'Code Setting',
            icon  : 'icon-layers',
            weight: 0
        });

        msNavigationServiceProvider.saveItem('code-settings.elements.revenue-code', {
            title : 'Revenue Code',
            weight: 0,
            state : 'app.code-settings_revenue-code'
        });

        msNavigationServiceProvider.saveItem('code-settings.elements.bp-revenue-code', {
            title : 'Business Permit Revenue Code',
            weight: 1
        });

        msNavigationServiceProvider.saveItem('code-settings.elements.application-fee', {
            title : 'Application Fee',
            weight: 2
        });

        msNavigationServiceProvider.saveItem('code-settings.elements.penalties', {
            title : 'Penalties',
            weight: 3
        });

        msNavigationServiceProvider.saveItem('code-settings.elements.parking-fee', {
            title : 'Parking Fee',
            weight: 3
        });

        msNavigationServiceProvider.saveItem('code-settings.elements.land-rates', {
            title : 'Land Rates',
            weight: 4
        });

        msNavigationServiceProvider.saveItem('code-settings.elements.land-revenue', {
            title : 'Land Revenue',
            weight: 5
        });

        msNavigationServiceProvider.saveItem('code-settings.elements.land-survey', {
            title : 'Land Survey',
            weight: 6
        });
    }
})();
