(function ()
{
    'use strict';
    
    angular
        .module('app.westpokot-land-rates', [])
        .config(config);
    
    
    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider
            .state('app.westpokot-land-rates', {
                url    : '/land-rates',
                views  : {
                    'content@app': {
                        templateUrl: 'app/main/verve/land-rates/land-rates.html',
                        controller : 'LandRatesController as vm'
                    }
                },
                resolve: {
                    SampleData: function (apiResolver)
                    {
                        return apiResolver.resolve('sample@get');
                    }
                }
            });
    
        // Translation
        $translatePartialLoaderProvider.addPart('app/main/verve/land-rates');
        
        // Navigation
        msNavigationServiceProvider.saveItem('land-rates', {
            title : 'LAND RATES',
            group : true,
            weight: 4
        });

        msNavigationServiceProvider.saveItem('land-rates.elements', {
            title      : 'Land rates',
            icon       : 'icon-tile-four',
            state      : 'app.westpokot-land-rates',
            weight     : 1
        });
    }
})();
