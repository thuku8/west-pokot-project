(function ()
{
    'use strict';
    
    angular
        .module('app.westpokot-banking', [])
        .config(config);
    
    
    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider
            .state('app.westpokot-banking', {
                url    : '/banking',
                views  : {
                    'content@app': {
                        templateUrl: 'app/main/verve/banking/banking.html',
                        controller : 'BankingController as vm'
                    }
                },
                resolve: {
                    SampleData: function (apiResolver)
                    {
                        return apiResolver.resolve('sample@get');
                    }
                }
            });
    
        // Translation
        $translatePartialLoaderProvider.addPart('app/main/verve/banking');
        
        // Navigation
        msNavigationServiceProvider.saveItem('banking', {
            title : 'BANKING',
            group : true,
            weight: 2
        });

        msNavigationServiceProvider.saveItem('banking.elements', {
            title      : 'Banking',
            icon       : 'icon-tile-four',
            state      : 'app.westpokot-banking',
            /*stateParams: {
             'param1': 'page'
             },*/
            translation: 'SAMPLE.SAMPLE_NAV',
            weight     : 1
        });
        
        
        // msNavigationServiceProvider.saveItem('banking.elements', {
        //     title : 'Banking',
        //     icon  : 'icon-layers',
        //     weight: 0
        // });
    }
})();
