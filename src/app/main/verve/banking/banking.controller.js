(function ()
{
    'use strict';

    angular
        .module('app.westpokot-banking')
        .controller('BankingController', BankingController);

    /** @ngInject */
    function BankingController(SampleData)
    {
        var vm = this;

        // Data
        vm.helloText = SampleData.data.helloText;

        // Methods

        //////////
    }
})();
