(function ()
{
    'use strict';

    angular
        .module('app.dashboards', [
            'app.dashboards.project'
        ])
        .config(config);

    /** @ngInject */
    function config(msNavigationServiceProvider)
    {
        // Navigation
        msNavigationServiceProvider.saveItem('apps', {
            title : 'WEST POKOT',
            group : true,
            weight: 0
        });

        msNavigationServiceProvider.saveItem('dashboards', {
            title : 'Dashboards',
            icon  : 'icon-tile-four',
            weight: 1
        });

        msNavigationServiceProvider.saveItem('dashboards.elements', {
            title: 'Business Permits',
            state: 'app.dashboards_project'
        });
    }

})();
