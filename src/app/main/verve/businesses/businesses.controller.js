(function ()
{
    'use strict';

    angular
        .module('app.westpokot-businesses')
        .controller('BusinessesController', BusinessesController);

    /** @ngInject */
    function BusinessesController(SampleData)
    {
        var vm = this;

        // Data
        vm.helloText = SampleData.data.helloText;

        // Methods

        //////////
    }
})();
