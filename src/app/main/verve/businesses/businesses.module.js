(function ()
{
    'use strict';
    
    angular
        .module('app.westpokot-businesses', [])
        .config(config);
    
    
    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider
            .state('app.westpokot-businesses', {
                url    : '/businesses',
                views  : {
                    'content@app': {
                        templateUrl: 'app/main/verve/businesses/businesses.html',
                        controller : 'BusinessesController as vm'
                    }
                },
                resolve: {
                    SampleData: function (apiResolver)
                    {
                        return apiResolver.resolve('sample@get');
                    }
                }
            });
    
        // Translation
        $translatePartialLoaderProvider.addPart('app/main/verve/businesses');
        
        // Navigation
        msNavigationServiceProvider.saveItem('businesses', {
            title : 'BUSINESSES',
            group : true,
            weight: 3
        });

        msNavigationServiceProvider.saveItem('businesses.elements', {
            title      : 'Businesses',
            icon       : 'icon-tile-four',
            state      : 'app.westpokot-businesses',
            weight     : 1
        });
    }
})();
