(function ()
{
    'use strict';

    angular
        .module('app.westpokot.auth.login', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider.state('app.westpokot_auth_login', {
            url      : '/login',
            views    : {
                'main@'                       : {
                    templateUrl: 'app/core/layouts/content-only.html',
                    controller : 'MainController as vm'
                },
                'content@app.westpokot_auth_login': {
                    templateUrl: 'app/main/verve/auth/login/login.html',
                    controller : 'LoginController as vm'
                }
            },
	        resolve: {
		        Auth: function(Auth) {
			        return Auth;
		        }
	        },
            bodyClass: 'login'
        });

        // Translation
        $translatePartialLoaderProvider.addPart('app/main/verve/auth/login');
    }

})();
