(function ()
{
    'use strict';

    angular
        .module('app.westpokot.auth.login')
        .controller('LoginController', LoginController);

    /** @ngInject */
    function LoginController($location,Auth)
    {
        var vm = this;
          // Data
        vm.loginFormData = {
            username:"",
            password:""
        };

        // Methods
	    vm.login = login;

        //////////


	    /**
	     * Login Function
	     */
	    function login()
	    {
		    Auth.login({ 'username': vm.loginFormData.username, 'password': vm.loginFormData.password });
		    vm.loginFormData.password = null;
	    }

    }
})();
