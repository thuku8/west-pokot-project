(function ()
{
    'use strict';

    angular
        .module('app.westpokot.auth.register', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider.state('app.westpokot_auth_register', {
            url      : '/register',
            views    : {
                'main@'                          : {
                    templateUrl: 'app/core/layouts/content-only.html',
                    controller : 'MainController as vm'
                },
                'content@app.westpokot_auth_register': {
                    templateUrl: 'app/main/verve/auth/register/register.html',
                    controller : 'RegisterController as vm'
                }
            },
	        resolve: {
			    SubCountiesService: function(SubCountiesService) {
				    return SubCountiesService;
			    }
		    },
            bodyClass: 'register'
        });

        // Translation
        $translatePartialLoaderProvider.addPart('app/main/verve/auth/register');
    }
})();