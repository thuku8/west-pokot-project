(function ()
{
    'use strict';

    angular
        .module('app.westpokot.auth.register')
        .controller('RegisterController', RegisterController);

    /** @ngInject */
    function RegisterController($location,SubCountiesService)
    {
	    var vm = this;

        // Data
	    vm.registerForm = {};
	    vm.subCounties = [];

        //////////
	    // Methods

	    //////////
	    init();

	    /**
	     * Initialize the controller
	     */

	    function init() {
		    /*
			 get all subcounties
			 */

		    SubCountiesService.getSubCounties()
			    .success(function (res) {
				    vm.subCounties = res.subCountiesData;
				    console.log(vm.subCounties);
			    })
			    .error(function (err) {
				    vm.subCounties = [];
				    $mdToast.show(
					    $mdToast.simple()
						    .textContent("Problem getting the subcounties!!!")
						    .position('top right')
						    .hideDelay(3000)
				    );

			    });
	    }
    }
})();