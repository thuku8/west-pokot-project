(function ()
{
    'use strict';

    angular
        .module('app.transactions.revenue-collector', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider
            .state('app.transactions_revenue-collector', {
                url    : '/transactions/revenue-collector',
                views  : {
                    'content@app': {
                        templateUrl: 'app/main/verve/transactions/revenue-collector/revenue-collector.html',
                        controller : 'RevenueCollectorController as vm'
                    }
                },
                resolve: {
                    SampleData: function (apiResolver)
                    {
                        return apiResolver.resolve('sample@get');
                    },
                    DashboardData: function (apiResolver)
                    {
                        return apiResolver.resolve('dashboard.project@get');
                    },
                    Employees: function (apiResolver)
                    {
                        return apiResolver.resolve('tables.employees100@get');
                    },
                    RevenueCollectors: function(RevenueCollectors) {
                        return RevenueCollectors;
                    },
                    StreamsCategoriesCollectionsService: function(StreamsCategoriesCollectionsService) {
                        return StreamsCategoriesCollectionsService;
                    },
                    SubCountiesService: function(SubCountiesService) {
                        return SubCountiesService;
                    }
                },
              
                bodyClass: 'revenue-collector'
            });
  
        msNavigationServiceProvider.saveItem('transactions.elements.revenue-collector', {
            title : 'Revenue Collector',
            weight: 0,
            state : 'app.transactions_revenue-collector'
        });
    }
})();
