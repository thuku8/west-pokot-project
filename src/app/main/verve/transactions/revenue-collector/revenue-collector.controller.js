(function ()
{
    'use strict';

    angular
        .module('app.transactions.revenue-collector')
        .controller('RevenueCollectorController', RevenueCollectorController);

    /** @ngInject */
    function RevenueCollectorController($document,$rootScope,$http,$log,$scope, $interval,$mdToast, $mdSidenav,fuseTheming,SampleData,DashboardData,Employees,RevenueCollectors,SubCountiesService)
    {
        var vm = this;

        // Data
        vm.themes = fuseTheming.themes;
        vm.showRevenueTotalCollectors = true;
        vm.activateFiltersSubmitBtn = false;
        vm.selectedCountyFilter = {
            filter : 'County',
            countyName : '',
            countyId: ''
        };
        vm.dateFilterSelectorVisible = false;
        vm.revenueCollectorsData = [];

        vm.isShowStream = true;
        vm.employees = Employees;

        //date pickers
        vm.isOpen = false;
        vm.collectorsFilter = {
            startDate:"",
            endDate:"",
            subCounty: ''
        };

        vm.minDate = new Date(
            new Date().getFullYear() -5,
            new Date().getMonth(),
            new Date().getDate()
        );
        vm.maxDate = new Date();

        vm.dtOptions = {
            dom       : '<"top"f>rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
            pagingType: 'simple',
            autoWidth : false,
            responsive: true
        };

        // Widget 5
        vm.widgetRevCollectorData = {
            title       : "Revenue Collector's Table",
            data        : [],
	        totals      : []
        };


        // Horizontal bar chart
        vm.multiBarHorizontalChart = {
            options: {
                chart: {
                    type              : 'multiBarHorizontalChart',
                    height            : 450,
                    x                 : function (d)
                    {
                        return d.label;
                    },
                    y                 : function (d)
                    {
                        return d.value;
                    },
                    showControls      : false,
                    showValues        : true,
                    transitionDuration: 500,
                    xAxis             : {
                        showMaxMin: false
                    },
                    yAxis             : {
                        axisLabel : 'Total Collection',
                        tickFormat: function (d)
                        {
                            return d3.format(',.2f')(d);
                        }
                    }
                }
            },
            data   : [
                {
                    'key'   : '	Brian Franklin',
                    'color' : '#d62728',
                    'values': [
                        {
                            'label': 'Collectors',
                            'value': 500
                        }
                    ]
                },
                {
                    'key'   : 'Harry Robinson',
                    'color' : '#1f77b4',
                    'values': [
                        {
                            'label': 'Collectors',
                            'value': 420
                        }
                    ]
                },
                {
                    'key'   : 'Joshua Clark',
                    'color' : '#FF6414',
                    'values': [
                        {
                            'label': 'Collectors',
                            'value': 313
                        }
                    ]
                },
                {
                    'key'   : 'Martha Koskei',
                    'color' : '#71A28A',
                    'values': [
                        {
                            'label': 'Collectors',
                            'value': 767
                        }
                    ]
                },
                {
                    'key'   : 'Norma Roberts',
                    'color' : '#EFB200',
                    'values': [
                        {
                            'label': 'Collectors',
                            'value': 500
                        }
                    ]
                },
                {
                    'key'   : 'Brenda Githure',
                    'color' : '#91A453',
                    'values': [
                        {
                            'label': 'Collectors',
                            'value': 103
                        }
                    ]
                }
            ]
        };


        vm.pieChart = {
            title       : "Revenue Collectors Pie Chart",
            options: {
                chart: {
                    type              : 'pieChart',
                    height            : 500,
                    x                 : function (d)
                    {
                        return d.key;
                    },
                    y                 : function (d)
                    {
                        return d.y;
                    },
                    showLabels        : true,
                    transitionDuration: 500,
                    labelThreshold    : 0.01,
                    legend            : {
                        margin: {
                            top   : 5,
                            right : 35,
                            bottom: 5,
                            left  : 0
                        }
                    }
                }
            },
            config : {
                refreshDataOnly: true,
                deepWatchData  : true
            },
            data   : [
                {
                    key: 'Brian Franklin',
                    y  : 500
                },
                {
                    key: 'Harry Robinson',
                    y  : 420
                },
                {
                    key: 'Joshua Clark',
                    y  : 313
                },
                {
                    key: 'Martha Koskei',
                    y  : 767
                },
                {
                    key: 'Norma Roberts',
                    y  : 500
                },
                {
                    key: 'Brenda Githure',
                    y  : 103
                }
            ]
        };




        // Methods
        vm.showStreamFunc = showStreamFunc;
        vm.toggleSidenav = toggleSidenav;
        vm.changeSelect = changeSelect;
        vm.filterCollectorsData = filterCollectorsData;
	    vm.lessThanExpression = lessThanExpression;

        //////////
        init();

        //////////

        /**
         * Initialize the controller
         */

        function init()
        {
            RevenueCollectors.getRevenueCollectorsData()
                .success(function(res) {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent(res.message)
                            .position('top right')
                            .hideDelay(3000)
                    );
	                vm.widgetRevCollectorData.data = res.revCollectorsCollectionsdata;
	                vm.widgetRevCollectorData.totals = res.totalCollectionsData;
                })
                .error(function(err) {
                    // vm.widgetRevCollectorData.data = [];
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent("Problem getting the revenue collectors data!!!")
                            .position('top right')
                            .hideDelay(3000)
                    );

                });



            /*
             get all subcounties
             */

            SubCountiesService.getSubCounties()
                .success(function(res) {
                    vm.subCounties = res.subCountiesData;
                })
                .error(function(err) {
                    vm.subCounties = [];
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent("Problem getting the subcounties!!!")
                            .position('top right')
                            .hideDelay(3000)
                    );

                });
        }



        /*
         Filters for counties and dates
         */
        function changeSelect(){
            if((vm.collectorsFilter.subCounty)){
                if(!vm.collectorsFilter.startDate && !vm.collectorsFilter.endDate){
                    vm.activateFiltersSubmitBtn = true;
                }else if(!vm.collectorsFilter.startDate || vm.collectorsFilter.endDate){
                    vm.activateFiltersSubmitBtn = false;
                }else if(vm.collectorsFilter.startDate || !vm.collectorsFilter.endDate){
                    vm.activateFiltersSubmitBtn = false;
                }

            }

            if((vm.collectorsFilter.subCounty) && vm.collectorsFilter.startDate && vm.collectorsFilter.endDate){
                vm.activateFiltersSubmitBtn = true;
            }

            if( vm.collectorsFilter.startDate && vm.collectorsFilter.endDate){
                vm.activateFiltersSubmitBtn = true;
            }
        };


        /**
         * Toggles filter with true or empty string
         * @param filter
         */
        function showStreamFunc(showStream)
        {
            vm.isShowStream = showStream;

        }
        /**
         * Toggle sidenav
         *
         * @param sidenavId
         */
        function toggleSidenav(sidenavId)
        {
            $mdSidenav(sidenavId).toggle();
        }


        function filterCollectorsData(){

            vm.filterData = {
                startDate:"",
                endDate:"",
                subCounty:""

            };

            if(!vm.collectorsFilter.startDate){
                vm.filterData.startDate = "";
            }else{
                vm.filterData.startDate = formatDate(new Date(vm.collectorsFilter.startDate))
            }

            if(!vm.collectorsFilter.endDate){
                vm.filterData.endDate = "";
            }else{
                vm.filterData.endDate = formatDate(new Date(vm.collectorsFilter.endDate))
            }

            if(!vm.collectorsFilter.subCounty){
                vm.filterData.subCounty = "";
            }else{
                vm.filterData.subCounty = vm.collectorsFilter.subCounty
            }


            RevenueCollectors.getRevenueCollectorsData({ params: vm.filterData})
                .success(function(res) {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent(res.message)
                            .position('top right')
                            .hideDelay(3000)
                    );
                    vm.widgetRevCollectorData.data = res.revCollectorsCollectionsdata;
	                vm.widgetRevCollectorData.totals = res.totalCollectionsData;
                    // vm.pieChart.data = [];
                    // angular.forEach(vm.widgetRevenueStreamsCategoriesData.data.collectionsData, function(prod) {
                    //
                    //     if(prod.account__stream || prod.account__stream != undefined || prod.account__stream != null){
                    //         vm.pieChart.data.push({
                    //             key: prod.account__stream,
                    //             y: prod.dsum
                    //         });
                    //     }
                    //
                    // });
                })
                .error(function(err) {
                    // vm.widgetRevenueStreamsCategoriesData = [];
                    // vm.pieChart.data = [];
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent("Problem getting the revenue collectors data!!!")
                            .position('top right')
                            .hideDelay(3000)
                    );

                });
            vm.filterSelectorVisible = !vm.filterSelectorVisible;
            vm.toggleSidenav('revenue-collector-filter-sidenav');

        }

        function formatDate(date){
            return date.getFullYear()+"-"+ (date.getMonth()+1) + "-" + date.getDate();
        }

	    function lessThanExpression(obj) {
			//this filter can be checked later
		    return !obj.dsum < 0 | obj.username !='' | obj.username;

	    };
    }
})();
