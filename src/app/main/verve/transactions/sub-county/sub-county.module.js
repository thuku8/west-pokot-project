(function ()
{
    'use strict';

    angular
        .module('app.transactions.sub-county', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider
            .state('app.transactions_sub-county', {
                url    : '/transactions/sub-county',
                views  : {
                    'content@app': {
                        templateUrl: 'app/main/verve/transactions/sub-county/sub-county.html',
                        controller : 'SubCountyTransactionsController as vm'
                    }
                },
                resolve: {
                    SampleData: function (apiResolver)
                    {
                        return apiResolver.resolve('sample@get');
                    },
                    DashboardData: function (apiResolver)
                    {
                        return apiResolver.resolve('dashboard.project@get');
                    },
                    Employees: function (apiResolver)
                    {
                        return apiResolver.resolve('tables.employees100@get');
                    },
                    SubCountiesCollectionsService: function(SubCountiesCollectionsService) {
                        return SubCountiesCollectionsService;
                    }
                },
  
                bodyClass: 'sub-county'
            });
    
        // Translation
        $translatePartialLoaderProvider.addPart('app/main/verve/transactions');
        
        msNavigationServiceProvider.saveItem('transactions.elements.sub-county', {
            title : 'Sub County',
            weight: 1,
            state : 'app.transactions_sub-county'
        });
    }
})();
