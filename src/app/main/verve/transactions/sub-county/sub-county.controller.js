(function ()
{
    'use strict';

    angular
        .module('app.transactions.sub-county')
        .controller('SubCountyTransactionsController', SubCountyTransactionsController);

    /** @ngInject */
    function SubCountyTransactionsController($document,$rootScope,$http,$log,$scope, $interval,$mdToast, $mdSidenav,fuseTheming,SampleData,DashboardData,Employees,SubCountiesCollectionsService)
    {
        var vm = this;

        // Data
        vm.themes = fuseTheming.themes;
        vm.filterSelectorVisible = false;
        vm.dateFilterSelectorVisible = false;
        vm.showRevenueTotalSubCounties = true;
        vm.helloText = SampleData.data.helloText;
        vm.isShowStream = true;
        vm.employees = Employees;

        //date pickers
        vm.isOpen = false;
        vm.subCountyFilter = {
            startDate:"",
            endDate:""
        };
        vm.minDate = new Date(
            new Date().getFullYear() -5,
            new Date().getMonth(),
            new Date().getDate()
        );
        vm.maxDate = new Date();

        vm.dtOptions = {
            dom       : '<"top"f>rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
            pagingType: 'simple',
            autoWidth : false,
            responsive: true
        };

        // Sub County Data
        vm.widgetSubCountyData = {
            title       : 'Total Collections For Each Sub-County',
            data        : []
        };

        // bar chart
        vm.barChart = {

            title:"Sub County Bar Chart",
            options:{
                chart: {
                    type: 'discreteBarChart',
                    height: 650,
                    margin : {
                        top: 20,
                        right: 20,
                        bottom: 50,
                        left: 55
                    },
                    x: function(d) {
                        return d.label;
                    },
                    y: function(d) {
                        return d.value + (1e-10);
                    },
                    showValues: true,
                    valueFormat: function(d) {
                        return d3.format(',.0f')(d);
                    },
                    duration: 500,
                    xAxis: {
                        axisLabel: 'SubCounties'
                    },
                    yAxis: {
                        axisLabel: 'Collections',
                        axisLabelDistance: -10
                    },
                    showXAxis: false,
                }
            },
            data:[
                {
                    key: "Sub County Bar Chart",
                    values: []
                }
            ]

        };


        vm.pieChart = {
            title       : "Sub County Pie Chart",
            options: {
                chart: {
                    type              : 'pieChart',
                    height            : 500,
                    x                 : function (d)
                    {
                        return d.key;
                    },
                    y                 : function (d)
                    {
                        return d.y;
                    },
                    showLabels        : true,
                    showLegend        : false, // to hide legend
                    showControls      : true, // to hide controls
                    transitionDuration: 500,
                    labelThreshold    : 0.01,
                    legend            : {
                        margin: {
                            top   : 5,
                            right : 35,
                            bottom: 5,
                            left  : 0
                        }
                    }
                }
            },
            config : {
                refreshDataOnly: true,
                deepWatchData  : true
            },
            data   : []
        };




        // Methods
        vm.showStreamFunc = showStreamFunc;
        vm.filterSubCountyData = filterSubCountyData;
        vm.toggleSidenav = toggleSidenav;
        // vm.clearFilters = CardFilters.clear;
        // vm.filteringIsOn = CardFilters.isOn;

        //////////
        init();

        //////////

        /**
         * Initialize the controller
         */

        function init()
        {
            SubCountiesCollectionsService.getAllSubCountyData()
                .success(function(res) {
                    vm.widgetSubCountyData.data = res;
                    angular.forEach(vm.widgetSubCountyData.data.collectionsData, function(prod) {

                        if(prod.subcounty || prod.subcounty != undefined || prod.subcounty != null || prod.sum < 0){
                            vm.pieChart.data.push({
                                key: prod.subcounty,
                                y: prod.dsum
                            });

                            vm.barChart.data[0].values.push({
                                label: prod.subcounty,
                                value: prod.dsum
                            });
                        }

                    });
                })
                .error(function(err) {
                    vm.pieChart.data =[];
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent("Problem getting the revenue collectors data!!!")
                            .position('top right')
                            .hideDelay(3000)
                    );

                });
        }

        /**
        * Toggles filter with true or empty string
        * @param filter
        */
        function showStreamFunc(showStream)
        {
            vm.isShowStream = showStream;
        }



        /**
             * Toggle sidenav
             *
             * @param sidenavId
        */
        function toggleSidenav(sidenavId)
        {
            $mdSidenav(sidenavId).toggle();
        }
        /**
        * Filter Sub County Data
        *
        * @param startDate and endDate
        */
        function filterSubCountyData() {

            vm.filterData = {
                startDate:formatDate(new Date(vm.subCountyFilter.startDate)),
                endDate:formatDate(new Date(vm.subCountyFilter.endDate))
            };


            SubCountiesCollectionsService.getAllSubCountyData({ params: vm.filterData })
                .success(function(res) {
                    vm.widgetSubCountyData.data = res;
                    angular.forEach(vm.widgetSubCountyData.data.collectionsData, function(prod) {

                      if(prod.subcounty || prod.subcounty != undefined || prod.subcounty != null || prod.sum < 0){
                            vm.pieChart.data.push({
                                key: prod.subcounty,
                                y: prod.dsum
                            });

                            vm.barChart.data[0].values.push({
                                label: prod.subcounty,
                                value: prod.dsum
                            });
                        }

                    });
                })
                .error(function(err) {

                    $mdToast.show(
                        $mdToast.simple()
                            .textContent("Problem getting the revenue collectors data!!!")
                            .position('top right')
                            .hideDelay(3000)
                    );

                });
            vm.filterSelectorVisible = !vm.filterSelectorVisible;
            vm.toggleSidenav('sub-county-filter-sidenav');


        }


        function formatDate(date){
            return date.getFullYear()+"-"+ (date.getMonth()+1) + "-" + date.getDate();
        }
    }
})();
