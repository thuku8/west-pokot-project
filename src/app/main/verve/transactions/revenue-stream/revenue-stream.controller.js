(function ()
{
    'use strict';

    angular
        .module('app.transactions.revenue-stream')
        .controller('RevenueStreamController', RevenueStreamController);

    /** @ngInject */
    function RevenueStreamController($document,$rootScope,$http,$log,$scope, $interval,$mdToast, $mdSidenav,fuseTheming,SampleData,DashboardData,Employees,StreamsCategoriesCollectionsService,SubCountiesService)
    {
        var vm = this;
    
        // Data
        vm.themes = fuseTheming.themes;
        vm.helloText = SampleData.data.helloText;
        vm.isShowStream = true;
        vm.employees = Employees;
    
        vm.subCounties = [];
        vm.dateFilterSelectorVisible = false;
    
        //date pickers
        vm.isOpen = false;
        vm.revStreamsFilter = {
            startDate:"",
            endDate:"",
            subCounty: ''
        };
        vm.dateFilterSelectorVisible = false;
    
        vm.minDate = new Date(
            new Date().getFullYear() -5,
            new Date().getMonth(),
            new Date().getDate()
        );
        vm.maxDate = new Date();
    
        vm.dtOptions = {
            dom       : '<"top"f>rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
            pagingType: 'simple',
            autoWidth : false,
            responsive: true
        };
    
        // Widget 5
        vm.widgetRevenueStreamsData = {
            title       : "Revenue Streams",
            data        : [],
	        totals      : []
        };
    
        //5647915, 210510, 26351735, 22744093,6061330
        // bar chart
        vm.barChart = {
            title       : "Revenue Streams Bar Graph",
            data             : {
                labels: ['Bus park(Kangaroo)','Advert-Pavement','Advert-Street','Bus park(Canters)','Hawking'],
                series: [
                    [500, 420, 313, 767,899]
                ]
            },
            options          : {
                seriesBarDistance: 15
            },
            responsiveOptions: [
                ['screen and (min-width: 641px) and (max-width: 1024px)', {
                    seriesBarDistance: 10,
                    axisX            : {
                        labelInterpolationFnc: function (value)
                        {
                            return value;
                        }
                    }
                }],
                ['screen and (max-width: 640px)', {
                    seriesBarDistance: 5,
                    axisX            : {
                        labelInterpolationFnc: function (value)
                        {
                            return value[0];
                        }
                    }
                }]
            ]
        };
    
    
        vm.pieChart = {
            title       : "Revenue Streams Pie Chart",
            options: {
                chart: {
                    type              : 'pieChart',
                    height            : 500,
                    x                 : function (d)
                    {
                        return d.key;
                    },
                    y                 : function (d)
                    {
                        return d.y;
                    },
                    showLabels        : true,
                    transitionDuration: 500,
                    labelThreshold    : 0.01,
                    legend            : {
                        margin: {
                            top   : 5,
                            right : 35,
                            bottom: 5,
                            left  : 0
                        }
                    }
                }
            },
            config : {
                refreshDataOnly: true,
                deepWatchData  : true
            },
            data   : [
                {
                    key: 'Bus park(Kangaroo)',
                    y  : 1500
                },
                {
                    key: 'Advert-Pavement',
                    y  : 1120
                },
                {
                    key: 'Advert-Street',
                    y  : 613
                },
                {
                    key: 'Bus park(Canters)',
                    y  : 767
                },
                {
                    key: 'Hawking',
                    y  : 899
                }
            ]
        };
    
    
    
    
        // Methods
        vm.showStreamFunc = showStreamFunc;
        vm.toggleSidenav = toggleSidenav;
        vm.changeSelect = changeSelect;
        vm.filterStreamsData = filterStreamsData;
    
    
        //////////
        init();
    
        //////////
    
        /**
         * Initialize the controller
         */
    
        function init()
        {
            /*
             get all subcounties
             */
        
            SubCountiesService.getSubCounties()
                .success(function(res) {
                    vm.subCounties = res.subCountiesData;
                })
                .error(function(err) {
                    vm.subCounties = [];
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent("Problem getting the subcounties!!!")
                            .position('top right')
                            .hideDelay(3000)
                    );
                
                });
            
            /*
                get streams data
             */
            StreamsCategoriesCollectionsService.getAllStreamsData({ params: vm.filterData})
                .success(function(res) {
	                vm.widgetRevenueStreamsData.data = res.collectionsData;
	                vm.widgetRevenueStreamsData.totals = res.totals;
                    vm.pieChart.data = [];
                    // angular.forEach(vm.widgetRevenueStreamsData.data.collectionsData, function(prod) {
                    //
                    //     if(prod.account__stream || prod.account__stream != undefined || prod.account__stream != null){
                    //         vm.pieChart.data.push({
                    //             key: prod.account__stream,
                    //             y: prod.dsum
                    //         });
                    //     }
                    //
                    // });
                })
                .error(function(err) {
                    // vm.widgetRevenueStreamsData = [];
                    vm.pieChart.data = [];
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent("Problem getting the revenue streams data!!!")
                            .position('top right')
                            .hideDelay(3000)
                    );
            
                });
        }
    
        /*
         Filters for counties and dates
         */
        function changeSelect(){
            if((vm.revStreamsFilter.subCounty)){
                if(!vm.revStreamsFilter.startDate && !vm.revStreamsFilter.endDate){
                    vm.activateFiltersSubmitBtn = true;
                }else if(!vm.revStreamsFilter.startDate || vm.revStreamsFilter.endDate){
                    vm.activateFiltersSubmitBtn = false;
                }else if(vm.revStreamsFilter.startDate || !vm.revStreamsFilter.endDate){
                    vm.activateFiltersSubmitBtn = false;
                }
                
            }
    
            if((vm.revStreamsFilter.subCounty) && vm.revStreamsFilter.startDate && vm.revStreamsFilter.endDate){
                vm.activateFiltersSubmitBtn = true;
            }
    
            if( vm.revStreamsFilter.startDate && vm.revStreamsFilter.endDate){
                vm.activateFiltersSubmitBtn = true;
            }
        };
    
    
        /**
         * Toggles filter with true or empty string
         * @param filter
         */
        function showStreamFunc(showStream)
        {
            vm.isShowStream = showStream;
        
        }
        /**
         * Toggle sidenav
         *
         * @param sidenavId
         */
        function toggleSidenav(sidenavId)
        {
            $mdSidenav(sidenavId).toggle();
        }
    
    
        function filterStreamsData(){
        
            vm.filterData = {
                startDate:"",
                endDate:"",
                subCounty:""
            
            };
        
            if(!vm.revStreamsFilter.startDate){
                vm.filterData.startDate = "";
            }else{
                vm.filterData.startDate = formatDate(new Date(vm.revStreamsFilter.startDate))
            }
        
            if(!vm.revStreamsFilter.endDate){
                vm.filterData.endDate = "";
            }else{
                vm.filterData.endDate = formatDate(new Date(vm.revStreamsFilter.endDate))
            }
        
            if(!vm.revStreamsFilter.subCounty){
                vm.filterData.subCounty = "";
            }else{
                vm.filterData.subCounty = vm.revStreamsFilter.subCounty
            }
    
    
    
    
            /*
             get streams data
             */
            StreamsCategoriesCollectionsService.getAllStreamsData({ params: vm.filterData})
                .success(function(res) {
                    // vm.widgetRevenueStreamsData.data = res;
	                vm.widgetRevenueStreamsData.data = res.collectionsData;
	                vm.widgetRevenueStreamsData.totals = res.totals;
                    vm.pieChart.data = [];
                    // angular.forEach(vm.widgetRevenueStreamsData.data.collectionsData, function(prod) {
                    //
                    //     if(prod.account__stream || prod.account__stream != undefined || prod.account__stream != null){
                    //         vm.pieChart.data.push({
                    //             key: prod.account__stream,
                    //             y: prod.dsum
                    //         });
                    //     }
                    //
                    // });
                })
                .error(function(err) {
                    // vm.widgetRevenueStreamsData = [];
                    vm.pieChart.data = [];
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent("Problem getting the revenue streams data!!!")
                            .position('top right')
                            .hideDelay(3000)
                    );
            
                });
            vm.filterSelectorVisible = !vm.filterSelectorVisible;
            vm.toggleSidenav('revenue-streams-filter-sidenav');
        
        }
    
        function formatDate(date){
            return date.getFullYear()+"-"+ (date.getMonth()+1) + "-" + date.getDate();
        }



	    function lessThanExpression(obj) {
		    //this filter can be checked later
		    return !obj.dsum < 0 | obj.account !='' | obj.account | obj.account__accountname;

	    };
    }
})();
