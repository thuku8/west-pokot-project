(function ()
{
    'use strict';

    angular
        .module('app.transactions.revenue-stream', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider
            .state('app.transactions_revenue-stream', {
                url    : '/transactions/revenue-stream',
                views  : {
                    'content@app': {
                        templateUrl: 'app/main/verve/transactions/revenue-stream/revenue-stream.html',
                        controller : 'RevenueStreamController as vm'
                    }
                },
                resolve: {
                    SampleData: function (apiResolver)
                    {
                        return apiResolver.resolve('sample@get');
                    },
                    DashboardData: function (apiResolver)
                    {
                        return apiResolver.resolve('dashboard.project@get');
                    },
                    Employees: function (apiResolver)
                    {
                        return apiResolver.resolve('tables.employees100@get');
                    },
                    StreamsCategoriesCollectionsService: function(StreamsCategoriesCollectionsService) {
                        return StreamsCategoriesCollectionsService;
                    },
                    SubCountiesService: function(SubCountiesService) {
                        return SubCountiesService;
                    }
                },
    
                bodyClass: 'revenue-stream'
            });
  
        msNavigationServiceProvider.saveItem('transactions.elements.revenue-stream', {
            title : ' Revenue Stream',
            weight: 2,
            state : 'app.transactions_revenue-stream'
        });
    }
})();
