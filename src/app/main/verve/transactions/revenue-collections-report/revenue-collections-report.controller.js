(function ()
{
    'use strict';

    angular
        .module('app.transactions.revenue-collections-report')
        .controller('RevenueCollectionsReportController', RevenueCollectionsReportController);

    /** @ngInject */
    function RevenueCollectionsReportController($document,$rootScope,$http,$log,$scope, $interval,$mdToast, $mdSidenav,fuseTheming,SampleData,DashboardData,Employees,StreamsCategoriesCollectionsService,SubCountiesService)
    {
        var vm = this;

        // Data
        vm.themes = fuseTheming.themes;
        vm.showRevenueTotalStreamsCategories = true;
        vm.activateFiltersSubmitBtn = false;
        vm.dashboardData = DashboardData;
        vm.projects = vm.dashboardData.dashboard1.projects;
        vm.helloText = SampleData.data.helloText;
        vm.isShowStream = true;
        vm.employees = Employees;

        //selects
        vm.subCounties = [];


        //date pickers
        vm.isOpen = false;
        vm.categoriesFilter = {
            startDate:"",
            endDate:"",
            subCounty: ''
        };
        vm.dateFilterSelectorVisible = false;

        vm.minDate = new Date(
            new Date().getFullYear() -5,
            new Date().getMonth(),
            new Date().getDate()
        );
        vm.maxDate = new Date();

        vm.dtOptions = {
            dom       : '<"top"f>rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
            pagingType: 'simple',
            autoWidth : false,
            responsive: true
        };

        // Widget 5
        vm.widgetRevenueStreamsCategoriesData = {
            title       : "Revenue Stream Categories Table",
            data        : [],
            pieChartData:[]
        };

        // multiBarHorizontalChart
        vm.multiBarHorizontalChart = {
            title       : "Revenue Streams Catgories Bar Graph",
            options: {
                chart: {
                    type              : 'multiBarHorizontalChart',
                    height            : 650,
                    x                 : function (d)
                    {
                      return d.label;
                    },
                    y                 : function (d)
                    {
                      return d.value;
                    },
                    showControls      : false,
                    showValues        : true,
                    transitionDuration: 500,
                    xAxis             : {
                        showMaxMin: false
                    },
                    valueFormat: function(d) {
                        return d3.format(',.0f')(d);
                    },
                    yAxis             : {
                        axisLabel : 'Collections',
                        tickFormat: function (d)
                        {
                            return d3.format(',.2f')(d);
                        }
                    }
                }
            },
            data   : [
                {
                  'key'   : 'Categories Collections',
                  'color' : '#1f77b4',
                  'values': []
                }
            ]
        };



        vm.doughnutChart = {
            title       : "Revenue Categories Chart",
            labels: [],
            data  : [],
            options : {
                responsive: true,
                maintainAspectRatio: true
            }
        };


        vm.pieChart = {
            data   : []
        };




        // Methods
        vm.showStreamFunc = showStreamFunc;
        vm.toggleSidenav = toggleSidenav;
        vm.changeSelect = changeSelect;
        vm.filterData = filterData;
        //////////
        init();
        //////////

        /**
         * Initialize the controller
         */

        function init()
        {

            // vm.widgetRevenueStreamsCategoriesData.pieChartData = [];
            StreamsCategoriesCollectionsService.getAllCategoriesData()
                .success(function(res) {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent(res.message)
                            .position('top right')
                            .hideDelay(3000)
                    );
                    vm.widgetRevenueStreamsCategoriesData.data = res;

                    angular.forEach(vm.widgetRevenueStreamsCategoriesData.data.collectionsData, function(prod) {

                        if(prod.account__stream || prod.account__stream != undefined || prod.account__stream != null || prod.dsum < 0){
                            vm.pieChart.data.push({
                                key: prod.account__stream,
                                y: prod.dsum
                            });

                            vm.multiBarHorizontalChart.data[0].values.push({
                                label: prod.account__stream,
                                value: prod.dsum
                            });

                            vm.doughnutChart.labels.push(prod.account__stream);
                            vm.doughnutChart.data.push(prod.dsum);

                        }
                    });
                })
                .error(function(err) {
                    // vm.widgetRevenueStreamsCategoriesData = [];
                    vm.pieChart.data = [];
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent("Problem getting the revenue collectors data!!!")
                            .position('top right')
                            .hideDelay(3000)
                    );

                });



            /*
             get all subcounties
             */

            SubCountiesService.getSubCounties()
                .success(function(res) {
                    vm.subCounties = res.subCountiesData;
                })
                .error(function(err) {
                    vm.subCounties = [];
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent("Problem getting the subcounties!!!")
                            .position('top right')
                            .hideDelay(3000)
                    );

                });
        }

        /*
         Filters for counties and dates
         */
        function changeSelect(){

            if((vm.categoriesFilter.subCounty)){
                if(!vm.categoriesFilter.startDate && !vm.categoriesFilter.endDate){
                    vm.activateFiltersSubmitBtn = true;
                }else if(!vm.categoriesFilter.startDate || vm.categoriesFilter.endDate){
                    vm.activateFiltersSubmitBtn = false;
                }else if(vm.categoriesFilter.startDate || !vm.categoriesFilter.endDate){
                    vm.activateFiltersSubmitBtn = false;
                }

            }

            if((vm.categoriesFilter.subCounty) && vm.categoriesFilter.startDate && vm.categoriesFilter.endDate){
                vm.activateFiltersSubmitBtn = true;
            }

            if( vm.categoriesFilter.startDate && vm.categoriesFilter.endDate){
                vm.activateFiltersSubmitBtn = true;
            }
        };

        /**
         * Toggles filter with true or empty string
         * @param filter
         */
        function showStreamFunc(showStream)
        {
            vm.isShowStream = showStream;
        }

        /**
         * Toggle sidenav
         *
         * @param sidenavId
         */
        function toggleSidenav(sidenavId)
        {
            $mdSidenav(sidenavId).toggle();
        }


        function filterData(){

            vm.filterData = {
                startDate:"",
                endDate:"",
                subCounty:""

            };

            if(!vm.categoriesFilter.startDate){
                vm.filterData.startDate = "";
            }else{
                vm.filterData.startDate = formatDate(new Date(vm.categoriesFilter.startDate))
            }

            if(!vm.categoriesFilter.endDate){
                vm.filterData.endDate = "";
            }else{
                vm.filterData.endDate = formatDate(new Date(vm.categoriesFilter.endDate))
            }

            if(!vm.categoriesFilter.subCounty){
                vm.filterData.subCounty = "";
            }else{
                vm.filterData.subCounty = vm.categoriesFilter.subCounty
            }


            StreamsCategoriesCollectionsService.getAllCategoriesData({ params: vm.filterData})
                .success(function(res) {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent(res.message)
                            .position('top right')
                            .hideDelay(3000)
                    );
                    vm.widgetRevenueStreamsCategoriesData.data = res;
                    vm.pieChart.data = [];
                    angular.forEach(vm.widgetRevenueStreamsCategoriesData.data.collectionsData, function(prod) {

                        if(prod.account__stream || prod.account__stream != undefined || prod.account__stream != null || prod.dsum < 0){
                            vm.pieChart.data.push({
                              key: prod.account__stream,
                              y: prod.dsum
                            });

                            vm.multiBarHorizontalChart.data[0].values.push({
                              label: prod.account__stream,
                              value: prod.dsum
                            });

                            vm.doughnutChart.labels.push(prod.account__stream);
                            vm.doughnutChart.data.push(prod.dsum);
                        }

                    });
                })
                .error(function(err) {
                    // vm.widgetRevenueStreamsCategoriesData = [];
                    vm.pieChart.data = [];
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent("Problem getting the revenue collectors data!!!")
                            .position('top right')
                            .hideDelay(3000)
                    );

                });

            vm.filterSelectorVisible = !vm.filterSelectorVisible;
            vm.toggleSidenav('revenue-collections-report-filter-sidenav');
        }

        function formatDate(date){
            return date.getFullYear()+"-"+ (date.getMonth()+1) + "-" + date.getDate();
        }
    }
})();
