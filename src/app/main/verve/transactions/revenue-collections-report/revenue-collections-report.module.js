(function ()
{
    'use strict';

    angular
        .module('app.transactions.revenue-collections-report', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider
            .state('app.transactions_revenue-collections-report', {
                url    : '/transactions/revenue-collections-report',
                views  : {
                    'content@app': {
                        templateUrl: 'app/main/verve/transactions/revenue-collections-report/revenue-collections-report.html',
                        controller : 'RevenueCollectionsReportController as vm'
                    }
                },
                resolve: {
                    SampleData: function (apiResolver)
                    {
                        return apiResolver.resolve('sample@get');
                    },
                    DashboardData: function (apiResolver)
                    {
                        return apiResolver.resolve('dashboard.project@get');
                    },
                    Employees: function (apiResolver)
                    {
                        return apiResolver.resolve('tables.employees100@get');
                    },
                    StreamsCategoriesCollectionsService: function(StreamsCategoriesCollectionsService) {
                        return StreamsCategoriesCollectionsService;
                    },
                    SubCountiesService: function(SubCountiesService) {
                        return SubCountiesService;
                    }
                },

                bodyClass: 'revenue-collections-report'
            });


        msNavigationServiceProvider.saveItem('transactions.elements.revenue-collections-report', {
            title : ' Revenue Categories',
            weight: 3,
            state :'app.transactions_revenue-collections-report'
        });
    }
})();
