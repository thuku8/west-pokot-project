(function ()
{
    'use strict';

    angular
        .module('app.transactions.collection-summary-yearly')
        .controller('AnnualCollectionsSummaryController', AnnualCollectionsSummaryController);

    /** @ngInject */
    function AnnualCollectionsSummaryController($document,$rootScope,$http,$log,$scope, $interval,$mdToast, $mdSidenav,fuseTheming,
                                                SampleData,DashboardData,Employees,AnnualReportsTotalsService,SubCountyReportsTotalsService,SubCountiesService,StreamsReportsService)
    {
        var vm = this;
    
        // Data
        vm.themes = fuseTheming.themes;
        
        //left sidenavs
        vm.isShowTotal = true;
        vm.isShowStream = false;
        vm.isShowStreamCategories = false;
        vm.isShowSubcounty = false;
        
        //subcounties
        vm.subCounties = [];
        vm.years = ['2017','2015','2014','2013','2012','2011','2010'];
        
        //needs to be removed
        vm.hideForDev = true;
        
        
        vm.helloText = SampleData.data.helloText;
        vm.employees = Employees;
        vm.dashboardData = DashboardData;
        
            //selects
        vm.selectedAnnualFilter = {
            year : '',
            subCounty: ''
        };
    
        vm.maxDate = new Date();
    
        vm.dtOptions = {
            dom       : '<"top"f>rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
            pagingType: 'simple',
            autoWidth : false,
            responsive: true
        };
      
        // Widget 5
        vm.widgetAnnualSummaryData = {
            title       : "Total Collections",
            data        : vm.employees.annualSummaryData.data
        };
        
        //all monthly reports//all monthly reports//all monthly reports//all monthly reports
        vm.totalReportsFilter = {
            year : '',
            subCounty: ''
        };
    
        vm.totalMonthlySummaryData = {
            title       : "Total Collections Monthly Summary",
            data        : []
        };
        //all monthly reports//all monthly reports//all monthly reports//all monthly reports
        
        //subcounty reports//subcounty reports//subcounty reports//subcounty reports
        vm.widgetSubCountyMonthlySummaryData = {
            title       : "SubCounty Collections Monthly Summary",
            data        : []
        };
    
        vm.selectedSubCountyFilter = {
            year : '',
            subCounty: ''
        };
        //subcounty reports//subcounty reports//subcounty reports//subcounty reports
        
        //stream category reports//stream category reports//stream category reports
        vm.selectedStreamCategoryFilter = {
            year : '',
            subCounty: ''
        };
        
        vm.widgetStreamCategoriesMonthlySummaryData = {
            title       : "Stream Categories Collections Monthly Summary",
            data        : []
        };
        //stream category reports//stream category reports//stream category reports
    
        //stream category reports//stream category reports//stream category reports
        vm.selectedStreamFilter = {
            year : '',
            subCounty: ''
        };
    
        vm.widgetStreamsMonthlySummaryData = {
            title       : "Streams Collections Monthly Summary",
            data        : []
        };
        //stream category reports//stream category reports//stream category reports
        
      
        //5647915, 210510, 26351735, 22744093,6061330
        // bar chart
        vm.barChart = {
            title       : "Annual Summary Graph",
            data             : {
                labels: ['Jan', 'Feb', 'Mar', 'Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec'],
                series: [
                    [500, 420, 313, 767,500, 420, 313, 767,500, 420, 313, 767]
                ]
            },
            options          : {
                seriesBarDistance: 15
            },
            responsiveOptions: [
                ['screen and (min-width: 641px) and (max-width: 1024px)', {
                    seriesBarDistance: 10,
                    axisX            : {
                        labelInterpolationFnc: function (value)
                        {
                          return value;
                        }
                    }
                }],
                ['screen and (max-width: 640px)', {
                    seriesBarDistance: 5,
                    axisX            : {
                        labelInterpolationFnc: function (value)
                        {
                          return value[0];
                        }
                    }
                }]
            ]
        };
      
      
        vm.pieChart = {
            title       : "Sub County Pie Chart",
            options: {
                chart: {
                    type              : 'pieChart',
                    height            : 500,
                    x                 : function (d)
                    {
                        return d.key;
                    },
                    y                 : function (d)
                    {
                        return d.y;
                    },
                    showLabels        : true,
                    transitionDuration: 500,
                    labelThreshold    : 0.01,
                    legend            : {
                        margin: {
                          top   : 5,
                          right : 35,
                          bottom: 5,
                          left  : 0
                        }
                    }
                }
            },
            config : {
              refreshDataOnly: true,
              deepWatchData  : true
            },
            data   : [
                {
                  key: 'OL-KALOU',
                  y  : 500
                },
                {
                  key: 'OL-JORO OROK',
                  y  : 420
                },
                {
                  key: 'NORTH KINANGOP',
                  y  : 313
                },
                {
                  key: 'SOUTH KINANGOP',
                  y  : 767
                },
                {
                key: 'KIPIPIRI',
                y  : 899
                }
            ]
        };
    
    
        // Widget 1
        vm.widget1 = {
            title: vm.dashboardData.widget1.title,
            chart: {
                options: {
                    chart: {
                        type                   : 'lineChart',
                        color                  : ['#4caf50', '#3f51b5', '#ff5722'],
                        height                 : 320,
                        margin                 : {
                            top   : 32,
                            right : 32,
                            bottom: 32,
                            left  : 48
                        },
                        useInteractiveGuideline: true,
                        clipVoronoi            : false,
                        interpolate            : 'cardinal',
                        x                      : function (d)
                        {
                            return d.x;
                        },
                        y                      : function (d)
                        {
                            return d.y;
                        },
                        xAxis                  : {
                            tickFormat: function (d)
                            {
                                return d + ' min.';
                            },
                            showMaxMin: false
                        },
                        yAxis                  : {
                            tickFormat: function (d)
                            {
                                return d + ' MB';
                            }
                        },
                        interactiveLayer       : {
                            tooltip: {
                                gravity: 's',
                                classes: 'gravity-s'
                            }
                        },
                        legend                 : {
                            margin    : {
                                top   : 8,
                                right : 0,
                                bottom: 32,
                                left  : 0
                            },
                            rightAlign: false
                        }
                    }
                },
                data   : vm.dashboardData.widget1.chart
            }
        };
    
    
    
    
    
        // Methods
        vm.showDivsFunction = showDivsFunction;
        vm.changeSelect = changeSelect;
        vm.toggleSidenav = toggleSidenav;
        
        //////////
        init();
    
        //////////
    
        /**
         * Initialize the controller
         */
    
        function init()
        {
            vm.showDivsFunction(true,false,false,false);
            
            /*
                get all subcounties
             */
    
            SubCountiesService.getSubCounties()
                .success(function(res) {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent(res.message)
                            .position('top right')
                            .hideDelay(3000)
                    );
                    vm.subCounties = res.subCountiesData;
                })
                .error(function(err) {
                    vm.subCounties = [];
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent("Problem getting the subcounties!!!")
                            .position('top right')
                            .hideDelay(3000)
                    );
        
                });
        }
    
        /*
            Filters for counties and dates
         */
        function changeSelect(isShowTotal,isShowSubcounty,isShowStreamCategories,isShowStream){
            
            if(isShowTotal){
                $log.info("Show Totals");
            }else if(isShowSubcounty){
                $log.info("Show Subcounties Totals");
            }else if(isShowStreamCategories){
                $log.info("Show StreamCategories Totals");
            }else if(isShowStream){
                $log.info("Show Streams Totals");
            }else{
                $log.info("Unknown Filter");
            }
        };
        
        /**
        * Toggles filter with true or empty string
        * @param filter
        */
        function showDivsFunction(isShowTotal,isShowSubcounty,isShowStreamCategories,isShowStream)
        {
            vm.isShowTotal = isShowTotal;
            vm.isShowSubcounty = isShowSubcounty;
            vm.isShowStreamCategories = isShowStreamCategories;
            vm.isShowStream = isShowStream;
            
            if(vm.isShowSubcounty){
                
                //we should find a way of seeing if this had been initialized before
                
                SubCountyReportsTotalsService.getAllMonthlyData()
                    .success(function(res) {
                        // $mdToast.show(
                        //     $mdToast.simple()
                        //         .textContent(res.message)
                        //         .position('top right')
                        //         .hideDelay(3000)
                        // );
                        vm.widgetSubCountyMonthlySummaryData.data = res;
                    })
                    .error(function(err) {
                        vm.widgetSubCountyMonthlySummaryData.data = [];
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent("Problem getting the sub-county reports data!!!")
                                .position('top right')
                                .hideDelay(3000)
                        );
            
                    });
                //reset the filter
                vm.selectedSubCountyFilter = {
                    year : '',
                    subCounty: ''
                };
            }else if(vm.isShowStreamCategories){
                StreamsReportsService.getAllCategoriesMonthlyData()
                    .success(function(res) {
                        // $mdToast.show(
                        //     $mdToast.simple()
                        //         .textContent(res.message)
                        //         .position('top right')
                        //         .hideDelay(3000)
                        // );
                        vm.widgetStreamCategoriesMonthlySummaryData.data = res;
                    })
                    .error(function(err) {
                        vm.widgetStreamCategoriesMonthlySummaryData.data = [];
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent("Problem getting the streams categories report data!!!")
                                .position('top right')
                                .hideDelay(3000)
                        );
            
                    });
    
            }else if(vm.isShowStream){
                StreamsReportsService.getAllStreamsMonthlyData()
                    .success(function(res) {
                        // $mdToast.show(
                        //     $mdToast.simple()
                        //         .textContent(res.message)
                        //         .position('top right')
                        //         .hideDelay(3000)
                        // );
                        vm.widgetStreamsMonthlySummaryData.data = res;
                    })
                    .error(function(err) {
                        vm.widgetStreamsMonthlySummaryData.data = [];
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent("Problem getting the streams reports data!!!")
                                .position('top right')
                                .hideDelay(3000)
                        );
        
                    });
                
            }else{
                StreamsReportsService.getAllMonthlyCollectionsReports()
                    .success(function(res) {
                        // $mdToast.show(
                        //     $mdToast.simple()
                        //         .textContent(res.message)
                        //         .position('top right')
                        //         .hideDelay(3000)
                        // );
                        vm.totalMonthlySummaryData.data = res;
                    })
                    .error(function(err) {
                        vm.totalMonthlySummaryData.data = [];
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent("Problem getting the streams reports data!!!")
                                .position('top right')
                                .hideDelay(3000)
                        );
            
                    });
                
            }
            
            $log.info(isShowTotal);
        }
    
        /**
         * Toggle sidenav
         *
         * @param sidenavId
         */
        function toggleSidenav(sidenavId)
        {
            $mdSidenav(sidenavId).toggle();
        }
    }
})();
