(function ()
{
    'use strict';

    angular
        .module('app.transactions.collection-summary-yearly', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider
            .state('app.transactions_collection-summary-yearly', {
                url    : '/transactions/collection-summary-yearly',
                views  : {
                    'content@app': {
                        templateUrl: 'app/main/verve/transactions/collection-summary-yearly/collection-summary-yearly.html',
                        controller : 'AnnualCollectionsSummaryController as vm'
                    }
                },
                resolve: {
                    SampleData: function (apiResolver)
                    {
                        return apiResolver.resolve('sample@get');
                    },
                    DashboardData: function (apiResolver)
                    {
                        return apiResolver.resolve('dashboard.server@get');
                    },
                    Employees: function (apiResolver)
                    {
                        return apiResolver.resolve('tables.employees100@get');
                    },
                    AnnualReportsTotalsService: function(AnnualReportsTotalsService) {
                        return AnnualReportsTotalsService;
                    },
                    SubCountyReportsTotalsService: function(SubCountyReportsTotalsService) {
                        return SubCountyReportsTotalsService;
                    },
                    SubCountiesService: function(SubCountiesService) {
                        return SubCountiesService;
                    },
                    StreamsReportsService: function(StreamsReportsService) {
                        return StreamsReportsService;
                    }
                },
  
                bodyClass: 'collection-summary-yearly'
            });
  
        msNavigationServiceProvider.saveItem('transactions.elements.collection-summary-yearly', {
            title : 'Annual Summary',
            weight: 4,
            state : 'app.transactions_collection-summary-yearly'
        });
    }
})();
