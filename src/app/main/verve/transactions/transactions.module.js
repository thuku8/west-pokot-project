(function ()
{
  'use strict';
  
  angular
      .module('app.westpokot.transactions', [
          'app.transactions.revenue-collector',
          'app.transactions.sub-county',
          'app.transactions.revenue-stream',
          'app.transactions.revenue-collections-report',
          'app.transactions.collection-summary-yearly'
      ])
      .config(config);
  
  
  /** @ngInject */
  function config($stateProvider, $translatePartialLoaderProvider, msNavigationServiceProvider)
  {
    
      // Translation
      $translatePartialLoaderProvider.addPart('app/main/verve/transactions');
      
      // Navigation
      msNavigationServiceProvider.saveItem('transactions', {
        title : 'TRANSACTIONS',
        group : true,
        weight: 1
      });
      
      
      msNavigationServiceProvider.saveItem('transactions.elements', {
        title : 'Transactions',
        icon  : 'icon-layers',
        weight: 0
      });
      
  }
})();
