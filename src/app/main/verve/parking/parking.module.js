(function ()
{
    'use strict';
    
    angular
        .module('app.westpokot-parking', [])
        .config(config);
    
    
    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider
            .state('app.westpokot-parking', {
                url    : '/parking',
                views  : {
                    'content@app': {
                        templateUrl: 'app/main/verve/parking/parking.html',
                        controller : 'BusinessesController as vm'
                    }
                },
                resolve: {
                    SampleData: function (apiResolver)
                    {
                        return apiResolver.resolve('sample@get');
                    }
                }
            });
    
        // Translation
        $translatePartialLoaderProvider.addPart('app/main/verve/parking');
        
        // Navigation
        msNavigationServiceProvider.saveItem('parking', {
            title : 'PARKING',
            group : true,
            weight: 5
        });

        msNavigationServiceProvider.saveItem('parking.elements', {
            title      : 'Parking',
            icon       : 'icon-tile-four',
            state      : 'app.westpokot-parking',
            weight     : 1
        });
    }
})();
