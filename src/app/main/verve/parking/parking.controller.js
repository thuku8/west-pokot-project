(function ()
{
    'use strict';

    angular
        .module('app.westpokot-parking')
        .controller('ParkingController', ParkingController);

    /** @ngInject */
    function ParkingController(SampleData)
    {
        var vm = this;

        // Data
        vm.helloText = SampleData.data.helloText;

        // Methods

        //////////
    }
})();
