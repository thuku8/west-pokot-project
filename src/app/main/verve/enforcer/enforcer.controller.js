(function ()
{
    'use strict';

    angular
        .module('app.westpokot-enforcer')
        .controller('EnforcerController', EnforcerController);

    /** @ngInject */
    function EnforcerController(SampleData)
    {
        var vm = this;

        // Data
        vm.helloText = SampleData.data.helloText;

        // Methods

        //////////
    }
})();
