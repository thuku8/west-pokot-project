(function ()
{
    'use strict';
    
    angular
        .module('app.westpokot-enforcer', [])
        .config(config);
    
    
    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider
            .state('app.westpokot-enforcer', {
                url    : '/enforcer',
                views  : {
                    'content@app': {
                        templateUrl: 'app/main/verve/enforcer/enforcer.html',
                        controller : 'EnforcerController as vm'
                    }
                },
                resolve: {
                    SampleData: function (apiResolver)
                    {
                        return apiResolver.resolve('sample@get');
                    }
                }
            });
    
        // Translation
        $translatePartialLoaderProvider.addPart('app/main/verve/enforcer');
        
        // Navigation
        msNavigationServiceProvider.saveItem('enforcer', {
            title : 'ENFORCERS',
            group : true,
            weight: 6
        });

        msNavigationServiceProvider.saveItem('enforcer.elements', {
            title      : 'Enforcer Performance',
            icon       : 'icon-tile-four',
            state      : 'app.westpokot-enforcer',
            /*stateParams: {
             'param1': 'page'
             },*/
            translation: 'SAMPLE.SAMPLE_NAV',
            weight     : 1
        });
    }
})();
