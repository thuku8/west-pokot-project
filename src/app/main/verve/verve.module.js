
(function ()
{
    'use strict';

    angular
        .module('app.westpokot', [
            'app.dashboards',
            'app.westpokot.auth.login',
	        'app.westpokot.auth.register',
            'app.westpokot.transactions',
            'app.westpokot-banking',
            'app.westpokot-businesses',
            'app.westpokot-land-rates',
            'app.westpokot-parking',
            'app.westpokot-enforcer',
            'app.westpokot-code-settings',
            'ngStorage',
            'ui.router',
            'ngResource',
            'ngMaterial',
            'angular.filter'
        ])
        .config(config);
        /*,'ngResource','ngStorage','ngFileUpload','ngMaterial'*/
        /** @ngInject */
        function config(msNavigationServiceProvider)
        {
        
        }
})();
