(function ()
{
    'use strict';

    angular
        .module('app.toolbar')
	    .factory('loginService', function ($http, $location, $rootScope, $mdToast, $window,$localStorage,$log){
			return {
				logout: function(user) {

					return $http.post('http://159.203.68.204:8000/api/user_logout/', user)
						.success(function() {

							//check which one works
							delete $window.localStorage.token;
							delete $window.localStorage.currentUser
							delete $localStorage.token;
							delete $localStorage.currentUser;
							$rootScope.currentUser = null;

							$mdToast.show(
								$mdToast.simple()
									.textContent('You have been logged out...')
									.position('top right')
									.hideDelay(3000)
							);

							// $location.path('/login');
							$location.path('/login');
						})
						.error(function(response) {
							$mdToast.show(
								$mdToast.simple()
									.textContent('Sorry!!!There is a problem logging out...')
									.position('top right')
									.hideDelay(3000)
							);
						});

				}
			};

		})

	    .controller('ToolbarController', ['$rootScope', '$mdSidenav', '$translate', '$mdToast', '$log', '$localStorage', 'loginService',
		    ToolbarController
		]);

		/** @ngInject */
		function ToolbarController($rootScope, $mdSidenav, $translate, $mdToast,$log,$localStorage,loginService)
		{
			var vm = this;

			// Data
			vm.bodyEl = angular.element('body');
			$rootScope.global = {
				search: ''
			};

			// Methods
			vm.toggleSidenav = toggleSidenav;
			vm.logout = logout;


			//////////


			/**
			 * Toggle sidenav
			 *
			 * @param sidenavId
			 */
			function toggleSidenav(sidenavId)
			{
				$mdSidenav(sidenavId).toggle();
			}

			/**
			 * Logout Function
			 */
			function logout()
			{
				if($localStorage.currentUser){
					loginService.logout($localStorage.currentUser);
				}
				$log.info("We are logging out");
			}
		}


})();
