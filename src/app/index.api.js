(function ()
{
    'use strict';
    angular
        .module('west-pokot')
        .factory('api', apiService)

        .factory('RevenueCollectors', function($http) {
            return {
                getRevenueCollectorsData: function(data) {
                    return $http.get('http://159.203.68.204:8000/api/revCollectorsCollections/', data);
                }
            };
        }).factory('SubCountiesCollectionsService', function($http) {
            return {
                getAllSubCountyData: function(data) {
                    return $http.get('http://159.203.68.204:8000/api/subCountiesAllCollections/', data);
                }
            };
        }).factory('StreamsCategoriesCollectionsService', function($http) {
            return {
                getAllCategoriesData: function(data) {
                        return $http.get('http://159.203.68.204:8000/api/streamCategoriesAllCollections/', data);
                },
                getAllStreamsData: function (data) {
                    return $http.get('http://159.203.68.204:8000/api/streamsAllCollections/',data);
                }
            };
        }).factory('AnnualReportsTotalsService', function($http) {
            return {
                getAllData: function(data) {
                    return $http.get('http://159.203.68.204:8000/api/allMonthlyCollectionsReports/', data);
                }
            };
        }).factory('SubCountyReportsTotalsService', function($http) {
            return {
                getAllMonthlyData: function(data) {
                    return $http.get('http://159.203.68.204:8000/api/subCountyMonthlyCollectionsReports/', data);
                }
            };
        }).factory('StreamsReportsService', function($http) {
            return {
                getAllStreamsMonthlyData: function(data) {
                    return $http.get('http://159.203.68.204:8000/api/streamsMonthlyCollectionsReports/', data);
                },
                getAllCategoriesMonthlyData: function(data) {
                    return $http.get('http://159.203.68.204:8000/api/allStreamCategoriesMonthlyCollectionsReports/', data);
                },
                getAllMonthlyCollectionsReports: function(data) {
                    return $http.get('http://159.203.68.204:8000/api/allMonthlyCollectionsReports/', data);
                }
            };
        }).factory('SubCountiesService', function($http) {
            return {
                getSubCounties: function(data) {
                    return $http.get('http://159.203.68.204:8000/api/sub-counties/', data);
                }
            };
        })
	    //authentication
	    .factory('Auth', function($http, $location, $rootScope, $mdToast, $window,$localStorage) {


		    //var token = $window.localStorage.token;
		    var token = $localStorage.token;
		    if (token) {
			    var payload = JSON.parse($window.atob(token.split('.')[1]));
			    $rootScope.currentUser = payload.user;
		    }
		    return {

			    login: function(user) {
				    return $http.post('http://159.203.68.204:8000/api/login/',user)
					    .success(function(data) {

					    	//successful login
					    	if(data.result_code == 1){
							    var payload = JSON.parse($window.atob(data.token.split('.')[1]));
							    $rootScope.currentUser = angular.fromJson(payload.user);
							    // store username and token in local storage to keep user logged in between page refreshes
							    $localStorage.currentUser = angular.fromJson(payload.user);
							    $localStorage.token = data.token;
							    // add jwt token to auth header for all requests made by the $http service
							    $http.defaults.headers.common.Authorization = 'Bearer ' + data.token;

							    $mdToast.show(
								    $mdToast.simple()
									    .textContent('Cheers!You have logged in...')
									    .position('top right')
									    .hideDelay(3000)
							    );

							    $location.path('/dashboard');
						    }
						    //Your account is disabled.
						    if(data.result_code == 2){
							    $mdToast.show(
								    $mdToast.simple()
									    .textContent('Sorry!The account is no longer active.')
									    .position('top right')
									    .hideDelay(3000)
							    );
						    }
						    //Invalid login details
						    if(data.result_code == 3){
							    $mdToast.show(
								    $mdToast.simple()
									    .textContent('Sorry!The user login details are invalid.')
									    .position('top right')
									    .hideDelay(3000)
							    );
						    }
						    user.password = "";
					    })
					    .error(function(response) {
						    delete $localStorage.token;
						    // delete $localStorage.currentUser;

						    $mdToast.show(
							    $mdToast.simple()
								    .textContent('Error!There is a problem logging in!!!.')
								    .position('top right')
								    .hideDelay(3000)
						    );
					    });
			    },
			    register: function(user) {
				    return $http.post('http://192.168.0.37:3000/usersApi/userRegistration',user)
					    .success(function(data) {

						    $location.path('/ikoclass/login');
						    $mdToast.show(
							    $mdToast.simple()
								    .textContent('Congratulations!Your account has been created...')
								    .position('top right')
								    .hideDelay(3000)
						    );
					    })
					    .error(function(response) {
						    $mdToast.show(
							    $mdToast.simple()
								    .textContent('Error!!! '+response.data)
								    .position('top right')
								    .hideDelay(3000)
						    );
					    });

			    }
		    };
	    });

    /** @ngInject */
    function apiService($resource)
    {
        /**
         * You can use this service to define your API urls. The "api" service
         * is designed to work in parallel with "apiResolver" service which you can
         * find in the "app/core/services/api-resolver.service.js" file.
         *
         * You can structure your API urls whatever the way you want to structure them.
         * You can either use very simple definitions, or you can use multi-dimensional
         * objects.
         *
         * Here's a very simple API url definition example:
         *
         *      api.getBlogList = $resource('http://api.example.com/getBlogList');
         *
         * While this is a perfectly valid $resource definition, most of the time you will
         * find yourself in a more complex situation where you want url parameters:
         *
         *      api.getBlogById = $resource('http://api.example.com/blog/:id', {id: '@id'});
         *
         * You can also define your custom methods. Custom method definitions allows you to
         * add hardcoded parameters to your API calls that you want them to be sent every
         * time you make that API call:
         *
         *      api.getBlogById = $resource('http://api.example.com/blog/:id', {id: '@id'}, {
         *         'getFromHomeCategory' : {method: 'GET', params: {blogCategory: 'home'}}
         *      });
         *
         * In addition to these definitions, you can also create multi-dimensional objects.
         * They are nothing to do with the $resource object, it's just a more convenient
         * way that we have created for you to packing your related API urls together:
         *
         *      api.blog = {
         *          list     : $resource('http://api.example.com/blog);
         *          getById  : $resource('http://api.example.com/blog/:id', {id: '@id'});
         *          getByDate: $resource('http://api.example.com/blog/:date', {id: '@date'},
         *              'get': {method: 'GET', params: {getByDate: true}}
         *          );
         *      }
         *
         * If you look at the last example from above, we overrode the 'get' method to put a
         * hardcoded parameter. Now every time we make the "getByDate" call, the {getByDate: true}
         * object will also be sent along with whatever data we are sending.
         *
         * All the above methods are using standard $resource service. You can learn more about
         * it at: https://docs.angularjs.org/api/ngResource/service/$resource
         *
         * -----
         *
         * After you defined your API urls, you can use them in Controllers, Services and even
         * in the UIRouter state definitions.
         *
         * If we use the last example from above, you can do an API call in your Controllers and
         * Services like this:
         *
         *      function MyController (api)
         *      {
         *          // Get the blog list
         *          api.blog.list.get({},
         *
         *              // Success
         *              function (response)
         *              {
         *                  console.log(response);
         *              },
         *
         *              // Error
         *              function (response)
         *              {
         *                  console.error(response);
         *              }
         *          );
         *
         *          // Get the blog with the id of 3
         *          var id = 3;
         *          api.blog.getById.get({'id': id},
         *
         *              // Success
         *              function (response)
         *              {
         *                  console.log(response);
         *              },
         *
         *              // Error
         *              function (response)
         *              {
         *                  console.error(response);
         *              }
         *          );
         *
         *          // Get the blog with the date by using custom defined method
         *          var date = 112314232132;
         *          api.blog.getByDate.get({'date': date},
         *
         *              // Success
         *              function (response)
         *              {
         *                  console.log(response);
         *              },
         *
         *              // Error
         *              function (response)
         *              {
         *                  console.error(response);
         *              }
         *          );
         *      }
         *
         * Because we are directly using $resource servive, all your API calls will return a
         * $promise object.
         *
         * --
         *
         * If you want to do the same calls in your UI Router state definitions, you need to use
         * "apiResolver" service we have prepared for you:
         *
         *      $stateProvider.state('app.blog', {
         *          url      : '/blog',
         *          views    : {
         *               'content@app': {
         *                   templateUrl: 'app/main/apps/blog/blog.html',
         *                   controller : 'BlogController as vm'
         *               }
         *          },
         *          resolve  : {
         *              Blog: function (apiResolver)
         *              {
         *                  return apiResolver.resolve('blog.list@get');
         *              }
         *          }
         *      });
         *
         *  You can even use parameters with apiResolver service:
         *
         *      $stateProvider.state('app.blog.show', {
         *          url      : '/blog/:id',
         *          views    : {
         *               'content@app': {
         *                   templateUrl: 'app/main/apps/blog/blog.html',
         *                   controller : 'BlogController as vm'
         *               }
         *          },
         *          resolve  : {
         *              Blog: function (apiResolver, $stateParams)
         *              {
         *                  return apiResolver.resolve('blog.getById@get', {'id': $stateParams.id);
         *              }
         *          }
         *      });
         *
         *  And the "Blog" object will be available in your BlogController:
         *
         *      function BlogController(Blog)
         *      {
         *          var vm = this;
         *
         *          // Data
         *          vm.blog = Blog;
         *
         *          ...
         *      }
         */

        var api = {};

        // Base Url
        api.baseUrl = 'app/data/';

        api.dashboard = {
            project  : $resource(api.baseUrl + 'dashboard/project/data.json'),
            server   : $resource(api.baseUrl + 'dashboard/server/data.json'),
            analytics: $resource(api.baseUrl + 'dashboard/analytics/data.json')
        };

        api.cards = $resource(api.baseUrl + 'cards/cards.json');

        api.fileManager = {
            documents: $resource(api.baseUrl + 'file-manager/documents.json')
        };

        api.icons = $resource('assets/icons/selection.json');

        api.invoice = $resource(api.baseUrl + 'invoice/invoice.json');

        api.mail = {
            inbox: $resource(api.baseUrl + 'mail/inbox.json')
        };

        api.profile = {
            timeline    : $resource(api.baseUrl + 'profile/timeline.json'),
            about       : $resource(api.baseUrl + 'profile/about.json'),
            photosVideos: $resource(api.baseUrl + 'profile/photos-videos.json')
        };

        api.quickPanel = {
            activities: $resource(api.baseUrl + 'quick-panel/activities.json'),
            contacts  : $resource(api.baseUrl + 'quick-panel/contacts.json'),
            events    : $resource(api.baseUrl + 'quick-panel/events.json'),
            notes     : $resource(api.baseUrl + 'quick-panel/notes.json')
        };

        api.search = {
            classic : $resource(api.baseUrl + 'search/classic.json'),
            mails   : $resource(api.baseUrl + 'search/mails.json'),
            users   : $resource(api.baseUrl + 'search/users.json'),
            contacts: $resource(api.baseUrl + 'search/contacts.json')
        };

        api.scrumboard = {
            boardList: $resource(api.baseUrl + 'scrumboard/boardList.json'),
            board    : $resource(api.baseUrl + 'scrumboard/boards/:id.json')
        };

        api.tables = {
            employees   : $resource(api.baseUrl + 'tables/employees.json'),
            employees100: $resource(api.baseUrl + 'tables/employees100.json')
        };

        api.timeline = {
            page1: $resource(api.baseUrl + 'timeline/page-1.json'),
            page2: $resource(api.baseUrl + 'timeline/page-2.json'),
            page3: $resource(api.baseUrl + 'timeline/page-3.json')
        };

        api.todo = {
            tasks: $resource(api.baseUrl + 'todo/tasks.json'),
            tags : $resource(api.baseUrl + 'todo/tags.json')
        };



        api.sample = $resource(api.baseUrl + 'sample/sample.json');

        return api;
    }

})();
